package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lab2.dc.unifil.br.Classificadores;

import javax.crypto.spec.PSource;

public class Main {

    public static void main(String[] args) {
        // Realiza benchmarkings
        //comparacaoBubbleSelectInsertCrescente();
        //comparacaoBubbleSelectInsertDecrescente();
        //insertMergeCrescente();
        //insertMergeDecrescente();
        //mergeQuickCrescente();
        //mergeQuickDecrescente();
        comparaçãoMergesortOtimizado();
        //comparacaoLomutoHoare();

    }

    private static void comparacaoLomutoHoare() {
        //0,10000,500,500,
        //0,5000,100,500,
        //0,1000, 50, 1000,

        List<Medicao> medicoesLomuto = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,10000,500,500,
                FabricaListas::fabricarListaIntegersAleatoria,
                Classificadores::quicksortLomuto
                );
        List<Medicao> medicoesHoare = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,10000,500,500,
                FabricaListas::fabricarListaIntegersAleatoria,
                Classificadores::quicksortHoare
        );

        plotarGraficoDuplo("QuickSort Lomuto x Hoare",
                new String[]{"Lomuto", "Hoare"},
                medicoesLomuto,
                medicoesHoare
                );
    }

    private static void comparaçãoMergesortOtimizado() {
        int[] tamanhos = {1,2,4,8,16,32};
        List<List<Medicao>> allMedicoes = new ArrayList<>();
        for (int k : tamanhos) {
            List<Medicao> medicoes = Cronometro.benchmarkCrescimentoAlgoritmo(
                    0, 10000, 1000, 1000, k,
                    FabricaListas::fabricarListaIntegersAleatoria,
                    Classificadores::mergeSortOtimizado
            );
            allMedicoes.add(medicoes);
        }
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo("MergeSort otimizado p/ k = 1,2,4,8,16,32");
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (ms)");
        tt.setLegendas("k=1", "k=2", "k=4", "k=8", "k=16", "k=32" );
        List<Medicao> lista1 = allMedicoes.get(0);
        for (int i = 0; i < lista1.size(); i++) {
            Medicao amostra1 = allMedicoes.get(0).get(i);
            Medicao amostra2 = allMedicoes.get(1).get(i);
            Medicao amostra4 = allMedicoes.get(2).get(i);
            Medicao amostra8 = allMedicoes.get(3).get(i);
            Medicao amostra16 = allMedicoes.get(4).get(i);
            Medicao amostra32 = allMedicoes.get(5).get(i);
            tt.anotarAmostra(amostra1.getN(),
                    amostra1.getTempoSegundos(),
                    amostra2.getTempoSegundos(),
                    amostra4.getTempoSegundos(),
                    amostra8.getTempoSegundos(),
                    amostra16.getTempoSegundos(),
                    amostra32.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }

    private static void testeFabricarListas() {
        System.out.println(FabricaListas.fabricarListaIntegersAleatoria(10));
        System.out.println(FabricaListas.fabricarListaIntegersCrescente(10));
        System.out.println(FabricaListas.fabricarListaIntegersDecrescente(10));
    }

    private static void medicaoBogosort() {
        List<Medicao> medicoesBogosort = Cronometro.benchmarkCrescimentoAlgoritmo(
                1, 10, 1, 10,
                FabricaListas::fabricarListaIntegersAleatoria,
                Classificadores::bogosort
        );

        // Plotta gráfico com resultados levantados
        plotarGráfico("Bogosort",
                new String[]{"Bogosort"},
                medicoesBogosort);
    }

    private static void gerarGraficoOrdJava() {
        List<Medicao> medicoesJava = Cronometro.benchmarkCrescimentoAlgoritmo(
                10, 100, 5, 1000,
                FabricaListas::fabricarListaIntegersAleatoria,
                Classificadores::ordenarJava
        );
        plotarGráfico("Ordenação do Java",
                new String[]{"Java"},
                medicoesJava);
    }

    private static void comparacaoBubbleInsertCrescente() {
        List<Medicao> bubbleSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 25,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::bubbleSort);
        List<Medicao> insertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 25,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::insertionSort);

        // Plotta gráfico com resultados levantados
        plotarGraficoDuplo("Bubblesort x InsertionSort para listas crescentes",
                new String[]{"Bubble", "Insertion"},
                bubbleSort,
                insertionSort);
    }

    private static void comparacaoBubbleInsertDecrescente() {
        List<Medicao> bubbleSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                100000, 1000, 50,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::bubbleSort);
        List<Medicao> insertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                100000, 1000, 50,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::insertionSort);

        // Plotta gráfico com resultados levantados
        plotarGraficoDuplo("Bubblesort x InsertionSort para listas decrescentes",
                new String[]{"Bubble", "Insertion"},
                bubbleSort,
                insertionSort);
    }

    private static void comparacaoBubbleSelectInsertCrescente() {

        List<Medicao> bubbleSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 60,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::bubbleSort);
        List<Medicao> selectionSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 60,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::selectionSort);
        List<Medicao> insertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 60,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::insertionSort);

        // Plotta gráfico com resultados levantados
        plotarGraficoTriplo("Bubblesort x SelectionSort x InsertionSort para listas crescentes",
                new String[]{"Bubble", "Selection", "Insertion"},
                bubbleSort,
                selectionSort,
                insertionSort);
    }

    private static void comparacaoBubbleSelectInsertDecrescente() {

        List<Medicao> bubbleSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 60,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::bubbleSort);
        List<Medicao> selectionSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 60,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::selectionSort);
        List<Medicao> insertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(0,
                10000, 1000, 60,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::insertionSort);

        // Plotta gráfico com resultados levantados
        plotarGraficoTriplo("Bubblesort x SelectionSort x InsertionSort para listas decrescentes",
                new String[]{"Bubble", "Selection", "Insertion"},
                bubbleSort,
                selectionSort,
                insertionSort);
    }

    private static void insertMergeCrescente() {
        //0,10000,1000,150,
        List<Medicao> insertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,10000,1000,150,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::insertionSort);
        List<Medicao> mergeSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,10000,1000,150,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::mergeSort);

        plotarGraficoDuplo("InsertionSort x MergeSort para listas crescentes",
                new String[]{"Insertion", "Merge"},
                insertionSort,
                mergeSort);
    }

    private static void insertMergeDecrescente() {
        List<Medicao> insertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,10000,1000,150,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::mergeSort);
        List<Medicao> mergeSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,10000,1000,150,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::mergeSort);

        plotarGraficoDuplo("InsertionSort x MergeSort para listas decrescentes",
                new String[]{"Insertion", "Merge"},
                insertionSort,
                mergeSort);
    }

    private static void mergeQuickCrescente() {
        //0, 10000, 1000, 1000,
        List<Medicao> mergeSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0, 10000, 1000, 1000,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::mergeSort);
        List<Medicao> quickSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0, 10000, 1000, 1000,
                FabricaListas::fabricarListaIntegersCrescente,
                Classificadores::quicksortHoare);

        plotarGraficoDuplo("MergeSort x QuickSort para listas crescentes",
                new String[]{"Merge", "Quick"},
                mergeSort,
                quickSort);
    }

    private static void mergeQuickDecrescente() {
        List<Medicao> mergeSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,500,5,1000,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::mergeSort);

        List<Medicao> quickSort = Cronometro.benchmarkCrescimentoAlgoritmo(
                0,500,5,1000,
                FabricaListas::fabricarListaIntegersDecrescente,
                Classificadores::quicksortHoare);

        plotarGraficoDuplo("MergeSort x QuickSort para listas decrescentes",
                new String[]{"Merge", "Quick"},
                mergeSort,
                quickSort);
    }

    /**
     * Lê as medições e plota um gráfico.
     * @param titulo Título do gráfico.
     * @param legendas Legendas dos gráficos plotados.
     * @param medicoes Lista com as medições a serem plotadas.
     */
    private static void plotarGráfico(String titulo, String[] legendas,
                                      List<Medicao> medicoes) {
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo(titulo);
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (ms)");
        tt.setLegendas(legendas);
        for (int i = 0; i < medicoes.size(); i++) {
            Medicao amostraJava = medicoes.get(i);

            tt.anotarAmostra(amostraJava.getN(),
                    amostraJava.getTempoSegundos());
        }
        tt.exibirGraficoXY();
    }

    /**
     * Plota gráfico de comparação entre duas medições
     * @param titulo Título do gráfico.
     * @param legendas Legendas dos gráficos.
     * @param medicoes1 Primeira lista de medições a serem plotadas.
     * @param medicoes2 Segunda lista de medições a serem plotadas.
     */
    private static void plotarGraficoDuplo(String titulo, String[] legendas,
                                           List<Medicao> medicoes1,
                                           List<Medicao> medicoes2) {
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo(titulo);
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (ms)");
        tt.setLegendas(legendas);
        for (int i = 0; i < medicoes1.size(); i++) {
            Medicao amostra1 = medicoes1.get(i);
            Medicao amostra2 = medicoes2.get(i);
            tt.anotarAmostra(amostra1.getN(),
                    amostra1.getTempoSegundos(),
                    amostra2.getTempoSegundos());
        }
        tt.exibirGraficoXY();
    }

    /**
     * Plota um grafico de comparação entre três medições.
     * @param titulo Título do gráfico.
     * @param legendas Legendas dos gráficos
     * @param medicoes1 Primeira lista de medições a serem plotadas.
     * @param medicoes2 Segunda lista de medições a serem plotadas.
     * @param medicoes3 Terceira lista de medições a serem plotadas.
     */
    private static void plotarGraficoTriplo(String titulo, String[] legendas,
                                            List<Medicao> medicoes1,
                                            List<Medicao> medicoes2,
                                            List<Medicao> medicoes3) {
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo(titulo);
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (ms)");
        tt.setLegendas(legendas);
        for (int i = 0; i < medicoes1.size(); i++) {
            Medicao amostra1 = medicoes1.get(i);
            Medicao amostra2 = medicoes2.get(i);
            Medicao amostra3 = medicoes3.get(i);
            tt.anotarAmostra(amostra1.getN(),
                    amostra1.getTempoSegundos(),
                    amostra2.getTempoSegundos(),
                    amostra3.getTempoSegundos());
        }
        tt.exibirGraficoXY();
    }

}
