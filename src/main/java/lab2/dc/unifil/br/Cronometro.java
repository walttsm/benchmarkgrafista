package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.List;
import java.util.function.*;

/**
 * Write a description of class lab2.dc.unifil.br.Cronometro here.
 * 
 * @author Ricardo Inacio Alvares e Silva
 * @version 20200519
 */
public class Cronometro {

    /**
     * Faz benchmarkings do algoritmo que recebe uma entrada N, inicialmente igual a nInicial,
     * até nFinal, incrementando a nPasso. Retorna uma lista com todas essas medições, classificada
     * do menor N ao maior.
     *
     * @param nInicial Valor escalar de N inicialmente.
     * @param nFinal Valor escalar de N que interrompe as medições.
     * @param nPasso Quantidade de incremento em N a cada iteração de medições.
     * @param repeticoes Quantidade de vezes que cada medição é feita. Quanto maior o valor,
     *                   mais precisa a medição, mas mais demorado o processo total.
     * @param fabricadorCobaias Método que cria estruturas de dados aceitas como entrada pelo
     *                          algoritmo em teste, com dimensão configurável para permitir o
     *                          benchmarking em crescimento.
     * @param algoritmo Algoritmo a ser testado.
     * @param <T> Tipo da estrutura de dados do algoritmo.
     * @return Lista com medições de tempo, classificada do menor ao maior N.
     */
    public static <T> List<Medicao> benchmarkCrescimentoAlgoritmo(
            int nInicial, int nFinal, int nPasso, int repeticoes,
            Function<Integer, T> fabricadorCobaias,
            Consumer<T> algoritmo ) {

        List<Medicao> medicoes = new ArrayList<>();
        for (Integer n = nInicial; n <= nFinal; n += nPasso) {
            Integer finalN = n;
            Supplier<T> recriadorCobaia = () -> fabricadorCobaias.apply(finalN);
            Medicao medicao = new Medicao(n, benchmarkAlgoritmo(recriadorCobaia, algoritmo, repeticoes));
            medicoes.add(medicao);
        }
        return medicoes;
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }

    public static <T> List<Medicao> benchmarkCrescimentoAlgoritmo(
            int nInicial, int nFinal, int nPasso, int repeticoes, int k,
            Function<Integer, T> fabricadorCobaias,
            ObjIntConsumer<T> algoritmo
            ) {

        List<Medicao> medicoes = new ArrayList<>();
        for (Integer n = nInicial; n <= nFinal; n += nPasso) {
            Integer finalN = n;
            Supplier<T> recriadorCobaia = () -> fabricadorCobaias.apply(finalN);
            Medicao medicao = new Medicao(n, benchmarkAlgoritmo(recriadorCobaia, algoritmo, repeticoes, k));
            medicoes.add(medicao);
        }
        return medicoes;
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }

    /**
     * Mede o tempo de execução do algoritmo.
     * @param recriadorCobaia Fornece os dados para o método.
     * @param algoritmo Define o algoritmo de ordenação que está sendo medido.
     * @param repeticoes Número de repetições feitas para se retirar o valor de tempo do benchmark.
     * @param <T> Tipo dos dados recebidos.
     * @return O menor tempo de execução dentre as repetições realizadas.
     */
    public static <T> double benchmarkAlgoritmo(Supplier<T> recriadorCobaia, Consumer<T> algoritmo, int repeticoes) {
        Cronometro cron = new Cronometro();
        double menorTempo = Double.POSITIVE_INFINITY;
        for (int i = 0; i < repeticoes; i++) {
            T cobaia = recriadorCobaia.get();
            cron.zerar();
            cron.iniciar();
            algoritmo.accept(cobaia);
            double ultimoTempo = cron.parar();

            menorTempo = Math.min(menorTempo, ultimoTempo);
        }

        return menorTempo;
    }

    public static <T> double benchmarkAlgoritmo(Supplier<T> recriadorCobaia, ObjIntConsumer<T> algoritmo, int repeticoes, int k) {
        Cronometro cron = new Cronometro();
        double menorTempo = Double.POSITIVE_INFINITY;
        for (int i = 0; i < repeticoes; i++) {
            T cobaia = recriadorCobaia.get();
            cron.zerar();
            cron.iniciar();
            algoritmo.accept(cobaia, k);
            double ultimoTempo = cron.parar();

            menorTempo = Math.min(menorTempo, ultimoTempo);
        }

        return menorTempo;
    }



    /**
     * Construtor padrão da classe.
     */
    public Cronometro() {
        this.start = 0;
        this.finish = 0;
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    /**
     * Inicia ou reinicia a contagem de tempo. Nunca zera o último estado do contador. Se o tempo já
     * estiver correndo, não faz nada.
     */
    public void iniciar() {
        this.start = (this.start != 0) ? this.start : System.nanoTime();

        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    /**
     * Para a contagem de tempo e retorna uma leitura do tempo decorrido.
     * 
     * @return Tempo decorrido até o momento da parada, em milissegundos.
     */
    public double parar() {
        this.finish = System.nanoTime();
        double time  = (this.finish - this.start) / 1e6;
        return time;
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    /**
     * Retorna o tempo decorrido contado até então, independente se está parado ou correndo. Não
     * altera o estado de contagem (parado/correndo).
     * 
     * @return Tempo decorrido contado pelo cronômetro, em milissegundos.
     */
    public double lerTempoEmMilissegundos() {
        return (System.nanoTime() - this.start) / 1e6;
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    /**
     * Zera o contador de tempo do cronômetro. Se o cronômetro estava em estado de contagem, ele é
     * parado.
     */
    public void zerar() {
        if (this.start != 0 && this.finish == 0) {
            parar();
        }
        this.start = 0;
        this.finish = 0;
        //throw new RuntimeException("O aluno ainda não implementou essa funcionalidade.");
    }
    
    // Atributos da classe são declarados aqui
    long start;
    long finish;
    private int k = 1;
}
