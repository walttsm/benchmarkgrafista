package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class Classificadores {

    /**
     * Ordena a lista usando método já implementado da classe List.
     * @param lista Lista a ser ordenada
     */
    public static void ordenarJava(List<Integer> lista) {
        lista.sort(null);
    }

    public static void bogosort(List<Integer> lista) {
        while (!isOrdenada(lista)) {
            int i, j; do {
                i = rng.nextInt(lista.size());
                j = rng.nextInt(lista.size());
            } while (lista.get(i) < lista.get(j));

            permutar(lista, i, j);
        }

    }

    /**
     * Ordena a lista de forma crescente usando ordenação por seleção.
     * Melhor caso: T(n) = n² + 5n + 4 -> Igual ao pior caso, pois percorre o array mesmo já ordenado.
     * Pior caso: T(n) = (n + 1) * (1 + n * 1 + 3) = (n + 1) * (n + 4) = n² + 5n + 4
     * Omega(n²), Theta(n²), O(n²)
     * @param lista Lista a ser ordenada
     */
    public static void selectionSort(List<Integer> lista) {
        for (int idxOrdenacao = 0; idxOrdenacao < lista.size(); idxOrdenacao++) { //-> n + 1
            int menor = idxOrdenacao;// -> 1
            for (int i = idxOrdenacao + 1 ; i < lista.size(); i++) { //-> n + 1 - 1 = n
                if (lista.get(i) < lista.get(menor)) menor = i; //-> 1
            }
            permutar(lista, idxOrdenacao, menor);//-> 3
        }
    }

    /**
     * Ordena a lista de forma crescente usando ordenação por bolha.
     * Melhor caso: T(n) = 1 * (1 + n + n + n + 3n + n) = 6n + 1
     * Pior caso:T(n) = n * (1 + n + n + 3n + n) = n * (6n + 1) = 6n² + n
     * Omega(n), Theta(n²), O(n²)
     * @param lista Lista a ser ordenada.
     */
    public static void bubbleSort(List<Integer> lista) {
        boolean permuta;
        do {
            permuta = false; //-> 1
            for (int i = 1; i < lista.size(); i++) { //->n + 1 - 1 -> n
                if (lista.get(i-1) > lista.get(i)) {//->1 * n -> n
                    permutar(lista, i - 1 , i);//->3 * n -> 3n
                    permuta = true; //-> 1 * n -> n
                }
            }
        } while (permuta);//pior caso -> n, melhor caso -> 1
    }

    /**
     * Ordena a lista de forma crescente usando ordenação por inserção.
     * Melhor caso: T(n) = (n + 1 - 1) * (1 + 1 + 1 * (1 + 1) + 1) = n * (3+2) = 5n
     * Pior caso: T(n) = (n + 1 - 1) * (1 + 1 + n * (1 + 1) + 1) = n * (3 + 2n) = 2n² + 3n
     * Omega(n), Theta(n²), O(n²)
     * @param lista Lista a ser ordenada
     */
    public static void insertionSort(List<Integer> lista) {
        int j; int eleito;
        for (int i = 1; i < lista.size(); i++) { //-> n + 1 - 1
            eleito = lista.get(i); //-> 1
            j = i-1; //-> 1
            while (j >= 0 && eleito < lista.get(j)) { // pior caso -> n, melhor caso -> 1
                lista.set(j + 1, lista.get(j));//-> 1
                j--;//-> 1
            }
            if (j != i - 1) lista.set(j + 1, eleito); //-> 1
        }
    }

    /**
     * Organiza uma parte de uma lista usando Insertionsort.
     * @param lista Lista sendo ser ordenada.
     * @param e Índice de início da ordenação.
     * @param d Índice de fim da ordenação.
     */
    public static void insertionSort(List<Integer> lista, int e, int d) {
        int j; int eleito;
        for (int i = e + 1; i < d; i++) {
            eleito = lista.get(i);
            j = i-1;
            while (j >= e && eleito < lista.get(j)) {
                lista.set(j + 1, lista.get(j));
                j--;
            }
            if (j != i - 1) lista.set(j + 1, eleito);
        }
    }

    /**
     * Ordena a lista de forma crescente usando ordenação por mergesort.
     * Omega(n * log(n)), Theta(n * log(n)), O(n * log(n))
     * Utiliza memoria auxiliar.
     * @param lista Lista a ser ordenada.
     */
    public static void mergeSort (List<Integer> lista) {
        if (lista.size() <= 1) {
            return;
        }

        int idxMeio = lista.size() / 2;
        List<Integer> esquerda = lista.subList(0, idxMeio);
        List<Integer> direita = lista.subList(idxMeio, lista.size());
        mergeSort(esquerda);
        mergeSort(direita);
        merge(lista, esquerda, direita);
    }

    /**
     * Ordena a lista de forma crescente usando ordenação por mergesort, mas ao atingir certo tamanho de lista,
     * ordena esta usando InsertionSort.
     * Utiliza memoria auxiliar.
     * @param lista Lista sendo ordenada.
     * @param k Tamanho limite de lista para ordenar usando insertionsort.
     */
    public static void mergeSortOtimizado(List<Integer> lista, int k) {
        if (lista.size() <= k) {
            insertionSort(lista);
            return;
        }

        int idxMeio = lista.size() / 2;
        List<Integer> esquerda = lista.subList(0, idxMeio);
        List<Integer> direita = lista.subList(idxMeio, lista.size());
        mergeSortOtimizado(esquerda, k);
        mergeSortOtimizado(direita, k);
        merge(lista, esquerda, direita);
    }

    /**
     * Método auxiliar que junta as listas esquerda e direita durante o mergesort crescente.
     * @param lista Lista sendo ordenada no escopo atual de execução.
     * @param esquerda Metade esquerda da lista a ser juntada.
     * @param direita Metade direita da lista a ser juntada.
     */
    private static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        List<Integer> copia = new ArrayList<>(lista);

        int idxE = 0; int idxD = 0; int idxCopia = 0;
        while (idxE <= esquerda.size() - 1 && idxD <= direita.size() - 1) {
            if (esquerda.get(idxE) < direita.get(idxD)) {
                copia.set(idxCopia, esquerda.get(idxE));
                idxE++;
            } else {
                copia.set(idxCopia, direita.get(idxD));
                idxD++;
            }
            idxCopia++;
        }

        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) {
            faltantes = esquerda;
            idxF = idxE;
        } else {
            faltantes = direita;
            idxF = idxD;
        }

        while (idxF < faltantes.size()) {
            copia.set(idxCopia, faltantes.get(idxF));
            idxCopia++; idxF++;
        }

        for (int i = 0; i < lista.size(); i++) {
            lista.set(i, copia.get(i));
        }

    }

    final private static Random rngQs = new Random();
    /**
     * Método de inicialização do quicksort com partição de Hoare. Não estável.
     * Omega(n * log(n)), Theta(n * log(n)), O(n²)
     * @param lista Lista a ser ordenada.
     */
    public static void quicksortHoare(List<Integer> lista) {
        quicksortHoare(lista, (vs) -> vs.get(rngQs.nextInt(vs.size())));
        //quicksort(lista, (vs) -> vs.size() / 2);
    }

    /**
     * Classifica a lista de forma crescente usando o método quicksort.
     * @param lista Lista a ser ordenada.
     * @param escolhedorPivo Função que escolhe o pivo para realização do quicksort.
     */
    public static void quicksortHoare(List<Integer> lista, Function<List<Integer>, Integer> escolhedorPivo) {
        // Caso base
        if (lista.size() <= 1) return;
        // Casos de subdivisão recursiva
        Integer pivo = escolhedorPivo.apply(lista);
        int idxPivo = reorganizarHoare(lista, pivo, (l, r) -> l.compareTo(r));
        quicksortHoare(lista.subList(0, idxPivo));
        quicksortHoare(lista.subList(idxPivo+1, lista.size()));
    }
    /**
     * Método auxiliar ao quicksort. Reorganiza a lista utilizando o método de Hoare.
     * @param lista Lista sendo ordenada.
     * @param pivo Pivô de organização.
     * @param cmp Comparador de Integers.
     * @return numero trocado
     */
    private static int reorganizarHoare(List<Integer> lista, Integer pivo, Comparator<Integer> cmp) {
        int l = 0, r = lista.size() - 1;
        while (true) {
            while (cmp.compare(lista.get(l), pivo) < 0) l++;
            while (cmp.compare(lista.get(r), pivo) > 0) r--;

            if (cmp.compare(lista.get(l), lista.get(r)) == 0) {
                if (r != 0) r--;
                while (cmp.compare(lista.get(r), pivo) > 0) r--;
            }


            if (l < r) {
                permutar(lista, l, r);
            } else return l;
        }
    }




    /**
     * Método de inicialização do quicksort com partição de Lomuto. Não estável
     * Omega(n * log(n)), Theta(n * log(n)), O(n²)
     * @param lista Lista a ser ordenada.
     */
    public static void quicksortLomuto(List<Integer> lista) {
        quicksortLomuto(lista, (vs) -> vs.get(rngQs.nextInt(vs.size())));
        //quicksort(lista, (vs) -> vs.size() / 2);
    }

    /**
     * Classifica a lista de forma crescente usando o método quicksort.
     * @param lista Lista a ser ordenada.
     * @param escolhedorPivo Função que escolhe o pivo para realização do quicksort.
     */
    public static void quicksortLomuto(List<Integer> lista, Function<List<Integer>, Integer> escolhedorPivo) {
        // Caso base
        if (lista.size() <= 1) return;
        // Casos de subdivisão recursiva
        Integer pivo = escolhedorPivo.apply(lista);
        int idxPivo = reorganizarHoare(lista, pivo, (l, r) -> l.compareTo(r));
        quicksortLomuto(lista.subList(0, idxPivo));
        quicksortLomuto(lista.subList(idxPivo+1, lista.size()));
    }
    private static int reorganizarLomuto(List<Integer> lista, Integer pivo, Comparator<Integer> cmp) {
        int l = 0;
        for (int i = l + 1; i <= lista.size() - 1; i++) {
            if (lista.get(i) < pivo) {
                permutar(lista, l, i);
                l++;
            }
        }
        permutar(lista, l + 1, lista.indexOf(pivo));
        return l + 1;

    }



    /**
     * Realiza a troca de dois elementos numa lista.
     * T(n) = 3
     * @param lista Lista onde ocorre a troca.
     * @param a Índice do primeiro elemento da troca.
     * @param b Índice do segundo elemento da troca.
     */
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a);
        lista.set(a, lista.get(b));
        lista.set(b, permutador);
    }

    /**
     * Verifica se a lista está ordenada.
     * @param lista Lista a ser verificada.
     * @return True se a lista estiver ordenada, senão, retorna false.
     */
    private static boolean isOrdenada(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++) {
            if (lista.get(i-1) > lista.get(i))
                return false;
        }
        return true;
    }

    private static Random rng = new Random("Seed constante repetível".hashCode());
}

enum Ordem {
    CRESCENTE, DECRESCENTE;
}
