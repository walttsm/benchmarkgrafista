package lab2.dc.unifil.br;

public class Medicao {

    /**
     * Construtor da classe Medicao.
     * @param n numero de elementos da medição.
     * @param tempoSegundos Tempo gasto em segundos para execução da tarefa.
     */
    public Medicao(int n, double tempoSegundos) {
        this.n = n;
        this.tempoSegundos = tempoSegundos;
    }

    /**
     * Retorna o numero de elementos da medição.
     * @return  Número de elementos da medição.
     */
    public int getN() {
        return n;
    }

    /**
     * Retorna o tempo em segundos da medição.
     * @return Tempo em segundos.
     */
    public double getTempoSegundos() {
        return tempoSegundos;
    }

    private int n;
    private double tempoSegundos;
}
